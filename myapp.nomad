job "myapp" {
  datacenters = ["dc1"]
  type        = "service"

  group "botapp" {
    count = 3




    task "myapp" {
      driver = "docker"
      config {
        image = "seymurnet/myapp:latest"

      }
      resources {
        cpu = 200
        memory = 128
      }
    }

  }
}
