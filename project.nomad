job "project" {
  ...

  group "project" {
    ...

    task "project" {
      ...
      driver = "docker"

      config {
        image   = "registry.gitlab.com/seymurnet/nomad-deployer:${CI_COMMIT_SHORT_SHA}"
        ...
      } # config
    } # task run
  } # group
} # job
